#!/usr/bin/env ruby
require 'syslog/logger'
require 'fileutils'
require 'imdb'

$source_drive = '/dev/sr0'
$output_dir = '/bitbox/Movies'
$temp_dir = '/tmp'
$encode_profile = 'High Profile' # see: https://trac.handbrake.fr/wiki/BuiltInPresets
$from_address = 'no-reply@email.com'
$recipients = ['recipient1@mail.com', 'recipient2@mail.com']
ENV['PATH'] = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

$log = Syslog::Logger.new 'autorip'

def identify_media
  $log.info 'identifying media'
  media_type = `cdrecord -v dev=#{$source_drive} -toc | grep 'Current:' | awk {'print $3'} | tr -d '()'`.chomp
  $log.info "detected #{media_type}"
  media_type
end


def eject_drive
  $log.info "ejecting drive"
  `eject #{$source_drive}`
end

def cleanup(item)
  $log.info "cleaning up #{item}"
  FileUtils.rm_rf item
end

def rip_audio
  $log.info "starting audio rip"
  system "cd #{$temp_dir}; abcde -c /root/.abcde.conf"
  $log.info "audio rip complete"
end

def check_for_running_encode
  begin
    running_job_id = `ps aux | grep H[a]ndBrakeCLI | awk {'print $2'}`.chomp
    raise if running_job_id.length > 0
  rescue
    $log.info "previous encode job [#{running_job_id}] running, sleeping for 30 seconds"
    sleep 30
    retry
  end
end

def encode_movie(extension, title, disc_name)
  begin
    $log.info "starting encode for #{title}"
    system "HandBrakeCLI --main-feature -i \"#{$temp_dir}/#{disc_name}\" -o \"#{$output_dir}/#{title}.#{extension}\" --preset=\"#{$encode_profile}\" 2>&1 | logger -t handbrake"
    unless $? == 0
      raise
    end
    $log.info "encode for #{title} complete"
  rescue
    $log.info 'encodeFailure encountered; sending notification and terminating execution'
    abort("encode failure")
  end
end

def get_disc_name
  `blkid -o value -s LABEL #{$source_drive}`.chomp
end

def get_movie_info(disc_name)
  begin
    $log.info "searching IMDB for #{disc_name}"
    raise unless movie = Imdb::Search.new(disc_name).movies[0]
    $log.info "found probable match: #{movie.title}"
  rescue
    $log.info "unable to find a match for #{disc_name}, returning a generic object"
    Struct.new('Movie', :title, :url, :poster)
    movie = Struct::Movie.new(disc_name, '#', 'https://s3.amazonaws.com/unixsherpa-public/Grumpy-Cat.jpg')
  end
  movie
end

def rip_title(title, disc_name)
  $log.info "starting rip for #{title}"
  system "mkdir \"#{$temp_dir}/#{disc_name}\""
  system "makemkvcon mkv --cache=16 --noscan -r --minlength=600 disc:0 all \"#{$temp_dir}/#{disc_name}\" 2>&1 | logger -t makemkv"
  $log.info "rip for #{title} completed with exit code #{$?.exitstatus}"
end

# main
if ENV['ID_CDROM_MEDIA']
  $log.info "starting autorip"
  media = identify_media

  case media
  when 'CD-ROM', 'CD-R'
    rip_audio
    eject_drive
  when 'DVD-ROM', 'DVD-R', 'BLURAY-ROM'
    disc_name = get_disc_name
    movie = get_movie_info disc_name
    rip_title movie.title, disc_name
    eject_drive
    check_for_running_encode
    encode_movie 'mkv', movie.title, disc_name
    cleanup "#{$temp_dir}/#{disc_name}"
  else
    $log.info "action not defined for media type #{media}"
    eject_drive
  end
end
